var config = require(__dirname + '/../bornevia-cloud-node/config.js');
var drivers = require(__dirname + '/../bornevia-cloud-node/drivers.js');
var utilities = require(__dirname + '/../bornevia-cloud-node/utilities.js');
var models  = require(__dirname + '/../bornevia-cloud-node/models');
var parseString = drivers.xml2js.parseString;
var https = require('https');
var xml = drivers.xml;
var fs = require('fs');

var self = this;
var app = drivers.express();

var redisSub = drivers.redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.password});
redisSub.setMaxListeners(0);
redisSub.select(config.redis.index);

//Using ssl certificate for accessing finesse service
var credentials = {
    key: fs.readFileSync(__dirname + '/key.pem', 'utf8'),
    cert: fs.readFileSync(__dirname + '/cert.crt', 'utf8'),
    passphrase: 'pidel1234567890'
};

/*app.configure(function() {
	app.set('port', process.env.PORT || 2080);
	app.use(drivers.bodyParser.json());
	app.use(drivers.bodyParser.urlencoded({ extended: false }));

});*/



https.createServer(credentials, app).listen(2080);
   console.log('Started!');
   process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

//get dialog id
app.get('/finesse', function(req, res) {
	var incomingData;
	var contacts;
	var contactID;
	var contactName;

	var visitorContact = [];
	var companyAccounts = [];
	var companies = [];

	var post = {};
	var ticket = {};

	var nowMoment = drivers.moment();
    var now = nowMoment.utc().toDate();
	drivers.async.series([
		function (callback){
		   	var dialogID = req.query.dialogID;
		   	contactID = req.query.contactID;
		   	//get call dialog
		   	//var finesseUrl = config.FINESSE_SERVER_URL+'/finesse/api/Dialog/'+dialogID; 
		   	var finesseUrl = 'https://localhost:2080/test'; 
		   	drivers.request(finesseUrl, function (error, response, body) {
				  if (!error && response.statusCode == 200) {
				  	parseString(body, function (err, result) {
		    			console.log('convert xml to json');
		    			incomingData = result;
		    			console.log(JSON.stringify(incomingData.Dialog.participants[0].Participant[0].startTime[0]));
		    			callback(null);
					});
				  }
				  else {
				  	console.log('Error get url ', error);
				  	callback(null);
				  }
			});
	   	},
	   	function (callback){
	   		/*var username = "administrator",
		    password = "Edavos99!",
		    url = config.FINESSE_SERVER_URL+'/finesse/api/PhoneBooks',
		    auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
		    drivers.request(
			    {
			        url : url,
			        headers : {
			            "Authorization" : auth
			        }
			    },
			    function (error, response, body) {
			        // Do more stuff with 'body' here
			        parseString(body, function (err, result) {

		   				console.log(JSON.stringify(result.PhoneBooks.PhoneBook));
		   				callback(null);
		   			});
			    }
			);*/
			var username = "administrator",
		    password = "Edavos99!",
		    url = config.FINESSE_SERVER_URL+'/finesse/api/PhoneBook/1/Contacts',
		    auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
		    drivers.request(
			    {
			        url : url,
			        headers : {
			            "Authorization" : auth
			        }
			    },
			    function (error, response, body) {
			        // Do more stuff with 'body' here
			        parseString(body, function (err, result) {
			        	contacts = result.Contacts.Contact;
		   				callback(null);
		   			});
			    }
			);
	   	},
	   	function(callback){
	   		models.company_accounts.findAll({
		    	where : {
		    		account_id : 2606 // hardcode the account id
		    	}
		  	}).then(function (result) {
		  		if(result.length > 0){
		  			companyAccounts = result;
		  		}
		  		console.log(1);
		  		callback(null);
		  	}).catch(function (err) {
		  		callback(err);
		  	});
	   	},
	   	function(callback){
	   		models.companies.findAll({
		    	where : {
		    		id : companyAccounts[0].company_id
		    	}
		  	}).then(function (result) {
		  		if(result.length > 0){
		  			companies = result;
		  		}
		  		callback(null);
		  	}).catch(function (err) {
		  		callback(err);
		  	});
	   	},
	   	function(callback){
	   		models.contacts.findAll({
		    	where : {
		    		name : req.query.contactName
		    	}
		  	}).then(function (result) {
		  		if(result.length > 0){
		  			visitorContact = result;
		  			contactName = visitorContact[0].name;
		  		}
		  		callback(null);
		  	}).catch(function (err) {
		  		callback(err);
		  	});
	   	},
	   	function(callback){
	   		if(visitorContact.length <= 0){
		   		for (var idx in contacts) {
				   if(contacts[idx].firstName[0] == req.query.contactName){
				   	    contactName = req.query.contactName;
				   	    break;
				   	    callback(null);
				   }
				}
				callback(null);
			} else {
				callback(null);
			}
	   	},
	   	function(callback){
	   		if(visitorContact.length <= 0){
	   			var contact = {};
                contact.company_id = 1;
                contact.name = contactName;
                contact.created_at = now;
                contact.updated_at = now;
	   			models.contacts.create(contact).then(function (result) {
	   				//visitorContact = result;
	   				visitorContact.push(result);
	   				callback(null);
	   			}).catch(function (err) {
                    callback(null);
                });
	   		} else {
	   			callback(null);
	   		}
	   	},
	   	function(callback){
	   		var text = 'Call duration = '+incomingData.Dialog.participants[0].Participant[0].startTime[0]+' until '+incomingData.Dialog.participants[0].Participant[0].endTime[0];
	   		post.channel = 'facebook';
            post.name = contactName;
            post.company_id = companyAccounts[0].company_id;
			post.message = 'You got call from '+ contactName + '<BR>' + text;
            post.subject = drivers.truncate('You got call from '+ contactName, config.POST_SUBJECT_LENGTH);
            post.post_type = 'facebook';
            post.account_id = companyAccounts[0].account_id;
            post.folder = 'inbox';
            post.opened = false;
            post.replied = false;
            post.liked = false;
            post.like_count = 0;
            post.contact_id = visitorContact[0].id;
            post.created_at = now;
            post.updated_at = now;
            post.posted_at = now;
            post.modified_at = now;
            post.can_delete = true;	

            ticket.company_id = companyAccounts[0].company_id;
            ticket.subject = post.subject;
            ticket.deleted = false;
            ticket.team_id = 307; // harcoding the team id
            ticket.status = 'Open';
            ticket.type = 'Task';
            ticket.contact_id = visitorContact[0].id;
            ticket.priority = 'Medium';
            ticket.updated_at = now;
            ticket.created_at = now;

            models.tickets.create(ticket).then(function(result) {
                ticket = result;
                post.ticket_id = ticket.id;
                console.log("Ticket created");
                models.posts.create(post).then(function(result) {
                    post = result;
                    console.log("Post created With message = "+post.message);
                    ticket.post_id = post.id;
                    callback(null);
                }).catch(function(err) {
                	callback(err);
                })
            }).catch(function(err) {
            	callback(err);
            });
	   	},
	   	function(callback){
		   	models.tickets.update({post_id : ticket.post_id}, {
	            where: {
	                id: ticket.id,
	                company_id : companyAccounts[0].company_id
	            }
	        }).then(function (result2) {
	            console.log("Ticket updated");
	            callback(null);
	        }).catch(function (err) {
	            console.log("Error when updating ticket, the reason = "+ err);
	            callback(null);
	        });
	   	},
	   	function(callback){
		   	models.tickets.findAll({
	            where: {
	                id: ticket.id
	            }
	        }).then(function (result) {
	        	ticket = result[0];
	            console.log("Ticket updated");
	            callback(null);
	        }).catch(function (err) {
	            console.log("Error when updating ticket, the reason = "+ err);
	            callback(null);
	        });
	   	},
	   	function(callback){
	   		models.notifications.create({
                company_id: companyAccounts[0].company_id,
                type: '/tickets',
                ticket_id: ticket.id,
                contact_id: post.contact_id,
                post_id: post.id,
                message: post.subject,
                opened: false,
                team_id: 307,
                created_at: now,
                updated_at: now
            }).then(function(result) {
                console.log("notification created");
                notification = result;
                callback(null);
            }).catch(function(err) {
                console.log("Error when creating notification, the reason = "+ err);
                callback(null);
            });  
	   	},
	   	function(callback){
	   		notification.channel = 'facebook';
            var stats = {
                notifications: [notification],
                company: companies
            }
            drivers.async.series([
                function (callback) {
                    drivers.redisPub.publish('/notifications', JSON.stringify(stats), function() {
                    	callback(null);
                    });
                }
            ],
            function (err, results) {
                callback(null);
            });
	   	},
	   	function(callback){
	   		ticket.post = post;
	   		var stats = {
                tickets: [ticket],
                company: companies
            }
            drivers.async.series([
                function (callback) {
                    utilities.completeTicketProperties(stats, null, callback);
                },
                function (callback) {
                    var hash = 'company:' + companyAccounts[0].company_id + ':team:' + ticket.team_id + ':contact:' + ticket.post.contact_id + ':tickets:all';
                    drivers.redisClient.multi().del(hash, function (err, result) {}).exec(function (err, replies) {
                        callback(null);
                    });
                },
                function (callback) {
                    drivers.redisPub.publish('/tickets', JSON.stringify(stats), function() {
                        callback(null);
                    });
                },
            ],
            function (err, results) {
                callback(null);
            });
	   	}
	],
    function (err, results) {
       //return drivers.response.error.custom("Verified!", 200, res);
       res.send('Verified!');
    });

});

/*app.listen(app.get('port'), function() {
 console.log('Server started on PORT:' + app.get('port') + '; Press Ctrl-C to terminate.');
});*/


app.get('/test', function(req, res){
   var ret = '<Dialog>'+
  '<mediaProperties>'+
  	'<DNIS>12131</DNIS>'+
  	'<callType>ACD_IN</callType>'+
  	'<callvariables>'+
  	'	<CallVariable>'+
  	'		<name>callVariable1</name>'+
  	'		<value/>'+
  	'	</CallVariable>'+
  	'	<CallVariable>'+
  	'		<name>callVariable2</name>'+
  	'		<value/>'+
  	'	</CallVariable>'+
  	'	<CallVariable>'+
  	'		<name>callVariable3</name>'+
  	'		<value/>'+
  	'	</CallVariable>'+
  	'</callvariables>'+
  '</mediaProperties>'+
  '<mediaType>Voice</mediaType>'+
  '<participants>'+
  '	<Participant>'+
  '		<actions>'+
  '			<action>CONSULT_CALL</action>'+
  '			<action>HOLD</action>'+
  '			<action>UPDATE_CALL_DATA</action>'+
  '			<action>SEND_DTMF</action>'+
  '			<action>START_RECORDING</action>'+
  '			<action>DROP</action>'+
  '		</actions>'+
  '		<mediaAddress>1050</mediaAddress>'+
  '		<mediaAddressType/>'+
  '		<startTime>2017-03-01T08:01:46.888z</startTime>'+
  '		<state>ACTIVE</state>'+
  '		<endTime>2017-03-01T08:01:46.888z</endTime>'+
  '	</Participant>'+
  '</participants>'+
'</Dialog>';
	
   res.set('Content-Type', 'text/xml');
   res.send(ret);
});

var convertDialogtoTicket = function(){
	//do something
	//requires complete documentation of finesse call dialog variable

}